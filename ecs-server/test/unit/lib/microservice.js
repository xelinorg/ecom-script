describe('testing the microservice lib module', function () {
  beforeEach(function () {
    // do something beforeEach it
  })

  it('it should test that the module get exposed as expected', function (done) {
    should(typeof server.lib.microservice).be.equal('object')
    done()
  })

  it('it should test that the module exposes the spawn function', function (done) {
    should(typeof server.lib.microservice.spawn).be.equal('function')
    done()
  })

  it('it should test that the spawned module is instanceof', function (done) {
    const spawnOpt = {
      port: process.env.PORT || 8888,
      http: http
    }
    const msrvInst = server.lib.microservice.Microservice
    const spawnedInst = server.lib.microservice.spawn(spawnOpt)
    should(spawnedInst instanceof msrvInst).be.equal(true)
    done()
  })
})

module.exports = {
  plugins: [
    "mocha"
  ],
  env: {
    commonjs: true,
    es6: true,
    node: true,
    mocha: true
  },
  extends: [
    'standard',
    'plugin:mocha/recommended'
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
    describe: 'readonly',
    beforeEach: 'readonly',
    it: 'readonly',
    server: 'readonly',
    should: 'readonly',
    http: 'readonly',
    descibe: 'readonly'
  },
  parserOptions: {
    ecmaVersion: 2018
  },
  rules: {
  }
}

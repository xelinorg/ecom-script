const path = require('path')
exports.app = require('./src').app
exports.domain = require('./src').domain
exports.lib = require('./src').lib
exports.getInstance = require('./src').getInstance
// process.argv[0] is node executable path
// if there is process.argv[1] it will be the full path of this scripts
// meaning that it has been invoked on command license
// the rest positions if exist are arguments passed at the command line
if (process.argv.length > 1) {
  console.log('ecs-server system call', process.argv)
  console.log('module location is', __filename)

  const nodebins = ['/app/.heroku/node/bin/node']
  if (process.env.NVM_BIN) {
    nodebins.push(process.env.NVM_BIN + '/node')
  }

  // check if node is the same with the one exported by nvm
  const nodeenv = nodebins.includes(process.argv[0])
  const isMe = __filename === process.argv[1]
  // test if the call is for starting mocha is calling this as well
  if (nodeenv && isMe) {
    const i = exports.getInstance()
    i.initPort()
    const frontend = path.resolve('client/build')
    console.log('frontend location is', frontend)
    i.expose({ ekey: '', evalue: i.static(frontend) })
  }
} else {
  console.log('ecs-server library call', process.argv)
}

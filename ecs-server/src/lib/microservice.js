
function Microservice (option) {
  'use strict'
  if (!(this instanceof Microservice)) {
    throw new Error('Microservice needs to be called with the new keyword')
  }
  const self = this

  // private state
  const rid = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)
  const srvproc = new option.http.Server()
  const port = option.port
  const name = option.name || rid
  const router = option.router || {} // TODO clone passed
  const expose = typeof option.expose === 'function' // TODO clone passed
  const use = []

  this.reader = option.reader

  // add an event listener for the request that uses the router
  srvproc.on('request', function (req, res) {
    // route has the leading slash removed
    const route = req.url.slice(1, req.url.length)

    // if there is no route registered get root that is all statics without mount point
    const routed = router[route]
    if (routed) return routed(req, res)

    const resolved = self.resolveUse(route)
    if (!routed && resolved) return resolved(req, res)

    // if we are here and we have served a static or any other registered route.. is bad
    res.writeHead(200, { 'Content-Type': 'application/json' })
    res.write(JSON.stringify({ 'hello ecs_i_': name }))
    res.end('\n')
  })

  this.resolveUse = function microserviceResolveUse (option) {
    const rKey = option.split('/')[0]
    const routed = router[rKey]
    // if routed is a mounted static we should remove the key somehow
    // we return the found static or the root route
    return routed || router['']
  }

  this.getStaticRoot = function microserviceGetStaticRoot (option) {
    const cleanPath = option.path.startsWith('/') ? option.path.substring(1) : option.path
    const rKey = cleanPath.split('/')[0]
    return Object.keys(router).includes(rKey) && option.path.indexOf('/' + rKey) === 0 ? rKey : ''
  }

  // exposing routes with provided or buildin dummy functionality
  this.expose = expose ? option.expose(router) : function (route) {
    const cleanKey = route.ekey.startsWith('/') ? route.ekey.substring(1) : route.ekey
    router[cleanKey] = route.evalue
  }

  // initialize use paths and midllwares
  this.initUse = function microserviceInitUse (option) {
    // should not accept subpaths registrations
    use.push(option)
  }

  // binding the port
  this.initPort = function microserviceInitPort () {
    srvproc.listen(port, function () {
      console.log(`ecs_${name} is listening on port ${port}`)
    })
  }
}

Microservice.prototype.initPoint = function microserviceInitPoint (option) {
  // make it easier to expose, could expose from array or more complex option
  return this.expose(option)
}

Microservice.prototype.static = function microserviceStatic (option) {
  // TODO implement static logic
  const reader = this.reader
  const payload = this.payload.bind(this)
  const getStaticRoot = this.getStaticRoot.bind(this)
  const errorResponse = this.readError.bind(this)
  return function microserviceStaticInner (req, res) {
    const staticMount = getStaticRoot({ mount: option, path: req.url })
    const absolutePath = req.url === '' || req.url === '/' ? option + '/index.html' : option + req.url.replace(staticMount, '').replace('//', '/')

    return reader().open({ path: absolutePath }, function (oerr, staticBlob) {
      if (oerr) return errorResponse(res, oerr)

      const blobData = staticBlob()
      const packed = payload({ path: req.url, size: blobData.length })

      res.writeHead(200, packed.headers)

      const body = packed.bin ? blobData : blobData.toString('utf8', 0, blobData.length)

      return res.end(body)
    })
  }
}

Microservice.prototype.use = function microserviceUse (useKey, useFunction) {
  const useBox = {}
  useKey && (useBox.ukey = useKey)
  useFunction && (useBox.uvalue = useFunction)
  this.initUse(useBox)
}

Microservice.prototype.readError = function microserviceReadError (res, error) {
  res.writeHead(400, { 'Content-Type': 'text/html' })
  let errorMessage = 'There was an error while processing your request'
  let errorCode = 500
  if (error.code === 'ENOENT') {
    errorMessage = 'Requested resource not found'
    errorCode = 400
  }
  const errorTemplate = `<!doctype html>
  <html lang="en">
    <head>
      <meta charset="utf-8"/>
      <link rel="shortcut icon" href="/favicon.ico"/>
      <meta name="viewport" content="width=device-width,initial-scale=1"/>
      <meta name="theme-color" content="#000000"/>
      <style>
     </style>
    </head>
    <body>
      <article>
        <h3><p>${errorMessage}</p></h3>
        <h6><p>${errorCode}</p></h6>
      </article>
    </body>
    </html>
  `

  res.write(errorTemplate)
  return res.end('\n')
}

Microservice.prototype.statStatic = function microserviceStatStatic (option) {
  return this.reader().stat(option)
}

Microservice.prototype.streamStatic = function microserviceStreamStatic (option) {
  // use stat more as will expose from
  const stat = this.reader().stat(option)

  option.res.writeHead(200, this.payload({ path: option.path, size: stat.size }))

  return this.reader().stream(option).pipe(option.res)
}

Microservice.prototype.payload = function microservicePayload (option) {
  const contentTypePrediction = this.extractContentType(option.path)
  return {
    headers: {
      'Content-Type': contentTypePrediction.mime,
      'Content-Length': option.size
    },
    bin: contentTypePrediction.bin
  }
}

Microservice.prototype.extractContentType = function microserviceExtractContentType (option) {
  const contentTypeRe = /^[a-zA-Z0-9/_.-]{1,512}\.([a-z]{1,5})$/g.exec(option)
  const contentTypeSwitch = {
    js: 'application/javascript',
    ico: 'image/x-ico',
    html: 'text/html',
    png: 'image/png',
    jpg: 'image/jpg',
    json: 'application/json',
    txt: 'text/plain'
  }
  // here we return text/plain if we can not get the mapping right, we sould log this
  const contentTypePicker = contentTypeRe && contentTypeRe.length === 2 ? contentTypeRe[1] : 'html'
  return {
    mime: contentTypeSwitch[contentTypePicker],
    bin: ['ico', 'png', 'jpg'].includes(contentTypePicker)
  }
}

exports.Microservice = Microservice

exports.spawn = function microserviceExportsSpawn (option) {
  return new Microservice(option)
}

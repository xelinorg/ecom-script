function Local (option) {
  this.fs = option.fs
}

Local.prototype.open = function localOpen (option) {
  const fsref = this.fs
  fsref.open(option.path, 'r', (oerr, fd) => {
    if (oerr) return option.reject(oerr)
    fsref.fstat(fd, (serr, stat) => {
      if (serr) return option.reject(serr)
      // use stat
      const buffer = Buffer.alloc(stat.size)
      // read its contents into buffer
      fsref.read(fd, buffer, 0, buffer.length, null, function (rerr, bytesRead, buffer) {
        if (rerr) return option.reject(rerr)
        const closer = function () {
          fsref.close(fd, (cerr) => {
            if (cerr) return option.reject(cerr)
          })
        }
        return option.resolve({ closer: closer, data: buffer })
      })
    })
  })
}

Local.prototype.stream = function localStream (option) {
  const fsref = this.fs
  fsref.open(option.path, 'r', (oerr, fd) => {
    if (oerr) throw oerr
    fsref.fstat(fd, (serr, stat) => {
      if (serr) throw serr
      // use stat
      const buffer = Buffer.alloc(stat.size)
      // read its contents into buffer
      fsref.read(fd, buffer, 0, buffer.length, null, function (rerr, bytesRead, buffer) {
        if (rerr) throw rerr
        // should stream here
        const data = buffer.toString('utf8', 0, buffer.length)
        option.resolve(data)
        // always close the file descriptor!
        fsref.close(fd, (cerr) => {
          if (cerr) throw cerr
        })
      })
    })
  })
}

Local.prototype.stat = function localStat (option) {
  const fsref = this.fs
  return fsref.open(option.path, 'r', (oerr, fd) => {
    if (oerr) return option.reject(oerr)

    fsref.fstat(fd, (serr, stat) => {
      if (serr) return option.reject(serr)

      // use stat
      const closer = function () {
        fsref.close(fd, (cerr) => {
          if (cerr) return option.reject(cerr)
        })
      }

      return option.resolve({ closer: closer, stat: stat })
    })
  })
}

exports.Local = Local

function Reader (option) {
  this.target = option.target
}

Reader.prototype.open = function readerOpen (handle, cb) {
  return this.target.open({
    path: handle.path,
    resolve: function (option) {
      option.closer()
      return cb(null, function () {
        return option.data
      })
    },
    reject: function (err) {
      return cb(err)
    }
  })
}

Reader.prototype.stat = function readerStat (handle, cb) {
  return this.target.stat({
    path: handle.path,
    resolve: function (option) {
      option.closer()
      return cb(null, function () {
        return option.stat
      })
    },
    reject: function (err) {
      return cb(err)
    }
  })
}

Reader.prototype.stream = function readerStream (handle) {
  return this.target.stream(handle)
}

exports.Reader = Reader

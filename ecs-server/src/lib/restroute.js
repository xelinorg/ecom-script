function breakPathPair (path) {
  const splitre = /(\w+|\d+)((^|\/\w+\d+^)|\/(\w+|\d+)|(\w+|\d+))/g
  return path.match(splitre).reduce(function (bucket, chunk) {
    const sliceIndex = chunk.indexOf('/')
    const key = chunk.slice(0, sliceIndex)
    const val = chunk.slice(sliceIndex + 1, chunk.length)
    const pair = {}
    pair[key] = val

    bucket.push(pair)
    return bucket
  }, [])
}

exports.expose = function (router) {
  return function restExposeInner (req) {

  }
}

exports.router = {
  global: {
    verb: {
      get: false,
      post: false,
      put: false,
      delete: false
    },
    monoresource: true,
    keytype: false
  }
}

exports.breakPathPair = breakPathPair

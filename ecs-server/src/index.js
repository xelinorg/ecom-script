// vendor
const http = require('http')
const fs = require('fs')

// inhouse
exports.app = require('./app')
exports.domain = require('./domain')
exports.lib = require('./lib')

exports.getReader = function () {
  const readerOpt = {
    target: new exports.lib.storage.local.Local({ fs: fs }),
    fs: fs
  }
  return new exports.lib.reader.Reader(readerOpt)
}

exports.getStorageLocal = function () {
  const spawnOpt = {
    fs: fs
  }
  return new exports.lib.storage.local.Local(spawnOpt)
}

exports.getInstance = function (option) {
  const port = option || 8888

  const spawnOpt = {
    port: process.env.PORT || port,
    http: http,
    reader: exports.getReader
  }
  return exports.lib.microservice.spawn(spawnOpt)
}

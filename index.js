exports.ecssrv = require('./ecs-server')

// process.argv[0] is node executable path
// if there is process.argv[1] it will be the full path of this scripts
// meaning that it has been invoked on command license
// the rest positions if exist are arguments passed at the command line
if (process.argv.length > 1) {
  console.log('ecom-script system call', process.argv)
} else {
  console.log('ecom-script library call', process.argv);

}

#!/bin/bash

THIS_SIDE=$1
FRONT_END="client"
BACK_END="ecs-server"

if [ "$THIS_SIDE" == "$BACK_END" ] || [ "$THIS_SIDE" == "$FRONT_END" ]; then
    echo "building $THIS_SIDE"
    catch() {
      echo "exiting badly :/"
      exit 1
    }
    cd $THIS_SIDE
    npm install
    npm run build
else
    echo "use bash build.sh server|client <<< one or the other not both :p"
    exit 1
fi

exit 0

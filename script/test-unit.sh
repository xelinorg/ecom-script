#!/bin/bash

THIS_SIDE=$1
FRONT_END="client"
BACK_END="ecs-server"

if [ "$THIS_SIDE" == "$BACK_END" ] || [ "$THIS_SIDE" == "$FRONT_END" ]; then
    echo "running unit tests for $THIS_SIDE"
    trap 'catch $LINENO' ERR
    catch() {
      echo "exiting badly :/"
      exit 1
    }
    cd $THIS_SIDE
    npm install
    npm test
else
    echo "use bash test-unit.sh server|client <<< one or the other not both :p"
    exit 1
fi

exit 0

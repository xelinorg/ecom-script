import React from "react";

const Navbar = () => {
  return (
    <nav>
      <a href="#">Nav 1</a>
      <a href="#">Nav 2</a>
      <a href="#">Nav 3</a>
    </nav>
  );
};

export default Navbar;

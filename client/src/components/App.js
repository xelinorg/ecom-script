import React from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import initLibrary from "../components/fontawesomes/initLibrary";
import TopBar from "./TopBar";

//initialize fontawesome library
initLibrary();

const App = () => {
  return (
    <>
      <CssBaseline />
      <header>Ecom - Headder</header>
      <TopBar />
      <section>
        <h2>Here is a Section</h2>
        <article>
          <p>
            {[...new Array(40)]
              .map(
                () => `Cras mattis consectetur purus sit amet fermentum.
Cras justo odio, dapibus ac facilisis in, egestas eget quam.
Morbi leo risus, porta ac consectetur ac, vestibulum at eros.
Praesent commodo cursus magna, vel scelerisque nisl consectetur et.`
              )
              .join("\n")}
          </p>
        </article>
      </section>
    </>
  );
};

export default App;

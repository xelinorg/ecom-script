import React from "react";
import PropTypes from "prop-types";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import useScrollTrigger from "@material-ui/core/useScrollTrigger";
import IconButtonBase from "./materials/IconButtonBase";
import { TYPE_FONTAWESOME, TYPE_SVG } from "./materials/iconTypes";

const ElevationScroll = props => {
  const { children } = props;
  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 0
  });

  return React.cloneElement(children, {
    elevation: trigger ? 4 : 0
  });
};

ElevationScroll.propTypes = {
  children: PropTypes.element.isRequired
};

const TopBar = props => {
  return (
    <ElevationScroll {...props}>
      <AppBar>
        <Toolbar>
          <Typography variant="h6" style={{ flex: 1 }}>
            Ecom
          </Typography>
          <IconButtonBase
            type={TYPE_FONTAWESOME}
            content="user"
            color="inherit"
          />
          <IconButtonBase
            type={TYPE_FONTAWESOME}
            content="cog"
            color="inherit"
          />
        </Toolbar>
      </AppBar>
    </ElevationScroll>
  );
};

export default TopBar;

/*
icon library creator
it should be called only once from App.js and create the library of the icons that 
are being used from the app.

TO-DO: make sure it's not being invoked in every re-render of the App component
*/
import { library } from "@fortawesome/fontawesome-svg-core";
// import "@fortawesome/free-brands-svg-icons"
// import "@fortawesome/free-regular-svg-icons"
import { faUser, faCog } from "@fortawesome/free-solid-svg-icons";
// import "@fortawesome/free-solid-svg-icons"

const initLibrary = () => {
  library.add(faUser, faCog);
};

export default initLibrary;

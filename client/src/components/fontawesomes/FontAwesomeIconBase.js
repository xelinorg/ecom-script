import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const FontAwesomeIconBase = ({ type = "fas", code, size, rotation }) => {
  return (
    <FontAwesomeIcon icon={[type, code]} size={size} rotation={rotation} />
  );
};

export default FontAwesomeIconBase;

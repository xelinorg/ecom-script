import React from "react";
import Icon from "@material-ui/core/Icon";
import SvgIcon from "@material-ui/core/SvgIcon";
import FontawesomeIconBase from "../fontawesomes/FontAwesomeIconBase";

export const FontawesomeIcon = (cls, content, color) => {
  return (
    <Icon className={cls} color={color}>
      <FontawesomeIconBase code={content} />;
    </Icon>
  );
};

export const MaterialIcon = (cls, content, color) => {
  return (
    <Icon className={cls} color={color}>
      {content}
    </Icon>
  );
};

export const VectorIcon = (cls, content, color) => {
  return (
    <SvgIcon className={cls} color={color}>
      <path d={content} />
    </SvgIcon>
  );
};

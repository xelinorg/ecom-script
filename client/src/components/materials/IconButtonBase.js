import React from "react";
import IconButton from "@material-ui/core/IconButton";
import { FontawesomeIcon, MaterialIcon, VectorIcon } from "./iconCreator";
import { TYPE_FONTAWESOME, TYPE_MATERIAL, TYPE_SVG } from "./iconTypes";

const renderByType = (type, cls, content) => {
  if (type === TYPE_FONTAWESOME) {
    return FontawesomeIcon(cls, content);
  }

  if (type === TYPE_MATERIAL) {
    return MaterialIcon(cls, content);
  }

  if (type === TYPE_SVG) {
    return VectorIcon(cls, content);
  }

  return null;
};

const IconButtonBase = ({ type, cls, content, color, args }) => {
  return (
    <IconButton color={color} {...args}>
      {renderByType(type, cls, content)}
    </IconButton>
  );
};

export default IconButtonBase;

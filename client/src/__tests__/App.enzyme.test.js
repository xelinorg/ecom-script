import React from "react";
import { shallow } from "enzyme";
import App from "../components/App";
import TopBar from "../components/TopBar";

describe("App React component test with Enzyme", () => {
  it("renders without crashing", () => {
    shallow(<App />);
  });

  it("expects App to have one TopBar component", () => {
    const wrapper = shallow(<App />);
    expect(wrapper.find(TopBar)).toHaveLength(1);
  });
});
